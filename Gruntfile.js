module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss', 'src/scss/modules', 'src/scss']
      },
      dist: {
        options: {
          outputStyle: 'expanded'
        },
        files: { // moved the scss files into a 'template' structure which mirrors the page template file structure
          'css/app.css': 'src/scss/app.scss'
        }        
      }
    },
    jade: {
        compile: {
            options: {
                client: false,
                pretty: true
            },
            files: [{
              expand: true,
              cwd: 'src/jade',
              src: [ '*.jade', '!_*.jade' ],
              dest: '',
              ext: '.html'
            },{
              expand: true,
              cwd: 'src/jade',
              src: 'therapy-services.jade',
              dest: 'imaging/',
              ext: '.html'
            },{
              expand: true,
              cwd: 'src/jade',
              src: 'forest-surgery-center.jade',
              dest: 'facilities/',
              ext: '.html'
            }]
        }
    },
    watch: {
      grunt: { files: ['Gruntfile.js'] },
      sass: {
        files: ['src/scss/*.scss', 'src/scss/modules/*.scss'],
        tasks: ['sass']
      },
      jade: {
        files: 'src/jade/*.jade',
        tasks: ['jade']
      }
    }
  });
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.registerTask('build', ['jade', 'sass']);
  // grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch']);
}