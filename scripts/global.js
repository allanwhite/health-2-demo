$(document).foundation({
  offcanvas : {
    open_method: 'move', // Sets method in which offcanvas opens, can also be 'overlap'
    close_on_click : false
  }
});
// Smoooooth scrolling between in-page links
Element.prototype.scrollIntoView = function() {
  $('html,body').animate({ scrollTop: $(this).offset().top-50 }, 500);
}

// this is a script to use html() to toggle content in buttons. The text content is defined in the onClick() event. 
$.fn.extend({
  toggleText:function(a,b){
      if(this.html()==a){this.html(b)}
      else{this.html(a)}
  }
});

$("#networks-info-toggle").click(function(){
  $("#networks-info").toggle();
  return false;
});
$("#reviews-info-toggle").click(function(){
  $("#reviews-info").toggle();
  return false;
});
$(".js-xtra-locations-toggle").click(function(){
  $("#xtra-locations").toggle();
  if (!$(this).hasClass('button')) window.location.href = windows.location.href + '#xtra-locations';
  return false;
});
$('#leave-review').click(function(){
  $('#review-login').fadeToggle();
  document.getElementById('review-login').style.display = "inline";
  return false;
});


//toggle details script -- needs to be cleaned up so it is more global

$('a.toggle-qm-details').click(function(e){
  e.preventDefault();
  $('.details').toggleClass('hide');
  var text = $(this).text();
  $(this).text(
  text == "Show more.." ? "Show less.." : "Show more..");
});

//replace text on focus - blur

$('input[type=text]').focus(function() {
    if (!$(this).data("DefaultText")) $(this).data("DefaultText", $(this).val());
    if ($(this).val() != "" && $(this).val() == $(this).data("DefaultText")) $(this).val("");
}).blur(function(){
    if ($(this).val() == "") $(this).val($(this).data("DefaultText"));
});