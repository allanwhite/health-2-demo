/**
 * Display elements with IDs matching this pattern:
 *   <term>-suggestions
 *
 * Also hide all suggestion lists before doing the above
 * in case any of them are already visible
 */
function displaySuggestions(value) {

  var id = '#' + value + '-suggestions';

  // Hide any visible selections
  $('.suggestion-results').addClass('hide');

  // Remove "hide" class from desired selections
  $(id).removeClass('hide');
}

function selectSuggestion(evt) {

  try {

    var $target = $(evt.target);

    if ($target.hasClass('suggest-item')) {

      // Set value of what box to suggestion text
      $('#input-what').val( $target.attr('data-suggestion') );

      // Calling this will hide the suggestions
      displaySuggestions();

      return false;
    }

  } catch(ex) {

    console.error(ex);
  }
}

// DOM Ready
$(function() {

  // Handle key up events for the what box
  $('#input-what').on('keyup', function() {

    try {

      displaySuggestions( this.value.toLowerCase() );

    } catch(ex) {

      console.error(ex);
    }
  });

  // Pass body clicks to "selectSuggestion" function
  $('body').on('click', selectSuggestion);
});